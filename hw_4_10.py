# Дан словарь: {'test':'test_value', 'europe':'eur',
# 'dollar':'usd', 'ruble':'rub'}. Добавить каждому ключу
# число равное длине этого ключа (пример{'key': 'value'} ->
# {'key3':'value'}). Чтобы получить список ключей- использовать метод .keys().
# подсказка-создается новый ключ с цифрой в конце, старый удаляется.

dict_1 = {'test':'test_value', 'europe':'eur','dollar':'usd', 'ruble':'rub'}
dict_2 = {}
for key, val in dict_1.items():
    dict_2[key + str(len(key))] = val
print(dict_2)








